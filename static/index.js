$(document).ready(function(){    
    //ancor scroll function

    $('a[href^="#"]').click(function(){
        $('html, body').animate({
            scrollTop: $( $.attr(this, 'href') )
            .offset().top
        }, 500);
        return false;
    });
    
    $(window).scroll(function() {
        var fromTop = $(this).scrollTop()
        // do we need comments here?
        if (fromTop > 110 && $('div#aboutRow').css('position') == "sticky") {
            $('div#aboutRow')
            .css({"transform": `translateX(${(fromTop-110)*(-1.6)}px)`})
        } else {
            $('div#aboutRow')
            .css({"transform": `translateX(0px)`});
        }
    });
})