const express = require('express');
const pug = require('pug');
const bodyParser = require('body-parser');

var app = express();
app.use(bodyParser.urlencoded({ extended: true }));

const port =  process.env.PORT || 3000;

app.set('view engine', 'pug')
app.use('/static', express.static('static'));


// TONS OF DATA
//will be in mongo later
var factList = [
    "I love burgers",
    "Most of my free time I spent on boards",
    "I'm ready work for food and knowledge"
]

var front_prj = [
    {
        title:"actual-design website",
        link:"http://actual-d.ru/",
        about:"web-site made with a team",
        used: "That was a team job, I've made most of layouts for prototype (looks almost same). Here was used pug.js, SCSS, custom grid. Slider made by click-sliders."
    },
    {
        title:"Portfolio site",
        link:"/",
        about: "You are here",
        used: "Well, here just bootstrap grid, pug.js, SCSS. Backend based on express.js(node)"
    }
    ]

var nodeprj = [
    {
        title:"Simple weather app",
        link:"https://shielded-fortress-88462.herokuapp.com",
        about:"Just a one day weather app",
        used : "Here I've used express.js, require.js and bordyParser libs, also google geocode and darksky.net API's"
    }
    ]

app.get('/', (req, res) =>{
    res.render('index', {
        factList,
        nodeprj,
        front_prj
    });
})

// //Вывод на коснольку во имя свтого
app.listen(port, () => {
    console.log(`Server is up on port ${port}`);
});